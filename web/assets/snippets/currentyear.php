<?php
// CopyYears - snippet for MODx
// Automatically updating copyright date
// License GPL
// Written 04-2006
 
// &startYear - first year of notice
//              defaults to current year
// [!CopyYears?startYear=`2005`!]
 
$spacer = " - ";
$now = date("Y");
$startYear = isset($startYear)? $startYear : $now;
$years = ($now > $startYear) ? $startYear.$spacer.$now : $now;
return "$years";