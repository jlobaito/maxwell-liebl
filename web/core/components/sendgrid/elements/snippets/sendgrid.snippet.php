<?php
/**
 * sendGrid snippet for sendGrid extra
 *
 * Copyright 2015 by Graeme Leighfield <http://www.gelstudios.co.uk>
 * Created on 02-22-2015
 *
 * sendGrid is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * sendGrid is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * sendGrid; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package sendgrid
 */

/**
 * Description
 * -----------
 * Route mail through sendGrid
 *
 * Variables
 * ---------
 * @var $modx modX
 * @var $scriptProperties array
 *
 * @package sendgrid
 **/

$tpl = $modx->getOption('sgTpl', $scriptProperties);
$mailSubject = $modx->getOption('sgSubject', $scriptProperties);
$mailFrom = $modx->getOption('sgFrom', $scriptProperties, $modx->getOption('emailsender'));
$mailTo = $hook->getValue($modx->getOption('sgTo', $scriptProperties, 'email'));
$mailToName = $hook->getValue($modx->getOption('sgToName', $scriptProperties, 'name'));
$mailFromName = $modx->getOption('sgFromName', $scriptProperties, $modx->getOption('site_name'));
$html = $modx->getChunk($tpl, $hook->getValues());

$url = 'https://api.sendgrid.com/';
$user = $modx->getOption('sendgrid_username');
$pass = $modx->getOption('sendgrid_password');

$params = array(
    'api_user'  => $user,
    'api_key'   => $pass,
    'to'        => $mailTo,
	'toname'	=> $mailToName,
	'fromname'	=> $mailFromName,
    'subject'   => $mailSubject,
    'html'      => $html,
    'from'      => $mailFrom,
);

$request =  $url.'api/mail.send.json';

// Generate curl request
$session = curl_init($request);
// Tell curl to use HTTP POST
curl_setopt ($session, CURLOPT_POST, true);
// Tell curl that this is the body of the POST
curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
// Tell curl not to return headers, but do return the response
curl_setopt($session, CURLOPT_HEADER, false);
// Tell PHP not to use SSLv3 (instead opting for TLS)
curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// obtain response
$response = curl_exec($session);
curl_close($session);

// print everything out
// $modx->log(xPDO::LOG_LEVEL_ERROR, print_r($response, true));

return true;