<?php
/**
 * en:default.inc.php topic lexicon file for sendGrid extra
 *
 * Copyright 2015 by Graeme Leighfield <http://www.gelstudios.co.uk>
 * Created on 02-22-2015
 *
 * sendGrid is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * sendGrid is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * sendGrid; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package sendgrid
 */

/**
 * Description
 * -----------
 * en:default.inc.php topic lexicon strings
 *
 * Variables
 * ---------
 * @var $modx modX
 * @var $scriptProperties array
 *
 * @package sendgrid
 **/



/* Used in transport.settings.php */
$_lang['setting_sendgrid_username'] = 'sendGrid Username';
$_lang['setting_sendgrid_username_desc'] = 'Your login username to sendGrid';
$_lang['setting_sendgrid_password'] = 'sendGrid Password';
$_lang['setting_sendgrid_password_desc'] = 'Your login password to sendGrid';