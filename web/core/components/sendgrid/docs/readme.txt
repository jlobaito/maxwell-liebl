This will send emails via sendGrid. Get a free account to route up to 400 emails per day. 
This is meant as a replacement for formItAutoResponder, albiet currently with less options.

Created due to certain mail services sending authorised/valid email to spam highly annoying! 
Ensure that your visitors get your autoresponders/emails by sending through sendGrid.

Free to use by anyone - Donated to the MODX community by GEL Studios Ltd. 

We love MODX, and we hope you do too!

Graeme Leighfield 
http://www.gelstudios.co.uk/