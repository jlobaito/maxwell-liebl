// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
=================================== */



$('.capabilities li:nth-child(even)').hover(
  function() {
    $('.bug span').css("color", "#fbad18");
  }, function() {
    $('.bug span').css("color", "#ededed");
  }
);

$('.capabilities li:nth-child(odd)').hover(
  function() {
    $('.bug span').css("color", "#0a4e9a");
  }, function() {
    $('.bug span').css("color", "#ededed");
  }
);

$('nav ul a').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top - 90
    }, 800);
    return false;
});

var navHeight = $('.mainnav').height();

$('.hero').css("margin-top", navHeight);
$('.cbp-spmenu ul').css("margin-top", navHeight + 20);



var navItems = jQuery('.mainnav a');
var win = jQuery(window);
var items = jQuery('section');


navItems.click(function(e){
    var item = jQuery(this);
    
    jQuery('.mainnav a.active').removeClass('active');
    item.addClass('active');
});


win.scroll(function(e){
    jQuery.each(items, function(key, value){
        var item = jQuery(value);
        if(win.scrollTop() >= item.offset().top - 500 ){
            jQuery('.mainnav a.active').removeClass('active');
            var id = item.attr('id');

            jQuery.each(navItems, function(key, value){
                var navItem = jQuery(value);
                if(navItem.attr('href') == '#'+id) navItem.addClass('active');
            });
        }
    });
});


/*
Reference: http://jsfiddle.net/BB3JK/47/
*/

$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

$( "iframe" ).wrap( "<div class='video-container'></div>" );
